/**
 * 
 * @param {String} str Строка, которую нужно дополнить слева
 * @param {String} addStr Строка или символ, коророй нужно дополнить
 * @param {Number} len Длина строки, до которой необходимо дополнять символы
 */
function repeateLeft(str, addStr, len) {
    var tmp = ('' + str).trim();
    while (tmp.length < len) {
        tmp = addStr + tmp;
    }
    return tmp;
}

function random() {
    return repeateLeft((Math.trunc(Math.random() * (255 - 0))).toString(16), 0, 2);
}

function separate(val) {
    return val.substr(0, 2) + ":" + val.substr(2, 2) + ':' + val.substr(4, 2) + ":" + val.substr(6, 2) + ":" + val.substr(8, 2) + ":" + val.substr(10, 2);
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

$(function () {
    $('form[name=mac48]').on('submit', function () {
        $(this).find(".result").remove();
        var glob = $(this).find('input[name=glob]').prop('checked');
        var vendor = parseInt($(this).find('select').val(), 16);
        if (vendor === 0) {
            vendor = repeateLeft(random(), 0, 2) + repeateLeft(random(), 0, 2) + repeateLeft(random(), 0, 2);
            if (glob) {
                vendor = repeateLeft((parseInt(vendor, 16) | 65536).toString(16), '0', 6);
            } else {
                vendor = repeateLeft((parseInt(vendor, 16) & 16711679).toString(16), '0', 6);
            }

        }
        vendor = repeateLeft(vendor, '0', 6);
        vendor += repeateLeft(random(), 0, 2) + repeateLeft(random(), 0, 2) + repeateLeft(random(), 0, 2);
        $(this).append('<div class=result>MAC48: ' + separate(vendor) + '</div>');
        $(this).append('<div class=result>UUIDv4: ' + uuidv4() + '</div>');
        return false;
    })
})