const gulp = require('gulp');
const stylus = require('gulp-stylus');
const sourcemaps = require('gulp-sourcemaps');
const gulpIf = require('gulp-if');
const del = require('del')
const inject = require('gulp-inject')
const rename = require('gulp-rename');
const path = require('path')
const htmlmin = require('gulp-htmlmin');
const through2 = require('through2').obj;
const webpackStream = require('webpack-stream');
const webpack = webpackStream.webpack;

const browserSync = require('browser-sync').create();

let isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';


gulp.task('styles', function () {
    return gulp.src('src/styles/style.styl')
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(stylus())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css'));
})

gulp.task('clean', function () {
    return del(['build', 'index.templ', 'index.html']);
})

gulp.task('js', function () {
    return gulp.src('src/js/*.js', { since: gulp.lastRun('js') })
        .pipe(gulp.dest('build/js'))
})

gulp.task('prep_html_opts', function () {
    return gulp.src('src/templ.txt')
        .pipe(rename({
            extname: ".html"
        }))
        .pipe(through2(function (file, enc, callback) {
            //console.log(file.contents);
            let arr = file.contents.toString('utf8').split('\n');
            let res = '<select name="prefix">\n<option value="000000" checked>Не определено</value>\n';
            for (let a of arr) {
                //console.log(a);
                let tmp = a.split(':');
                tmp[0] = tmp[0].trim();
                tmp[1] = tmp[1].trim().replace(/[\s\t]{2,100000000}/g, " ");
                res += `<option value="${tmp[0]}">${tmp[1]}</option>\n`;
            }
            res += "</select>\n";
            file.contents = new Buffer(res);
            //callback(null, res);
            callback(null, file)
        }))
        .pipe(gulp.dest('build'))
})

gulp.task('link', function () {
    return gulp.src('src/index.templ')
        .pipe(inject(gulp.src('build/css/*.css'), {
            starttag: '<!-- inject:styles -->',
            endtag: '<!-- endinjectstyle -->',
            removeTags: true,
            relative: true,
            transform: function (filePath, file) {
                return file.contents.toString('utf8');
            }
        }))
        .pipe(inject(gulp.src(['node_modules/jquery/dist/jquery.slim.min.js', 'build/js/*.js']), {
            starttag: '<!-- inject:js -->',
            endtag: '<!-- endinjectjs -->',
            removeTags: true,
            relative: true,
            transform: function (filePath, file) {
                return file.contents.toString('utf8');
            }
        }))
        .pipe(inject(gulp.src('build/templ.html'), {
            starttag: '<!-- inject:html_opts -->',
            endtag: '<!-- endinjecthtml -->',
            removeTags: true,
            relative: true,
            transform: function (filePath, file) {
                return file.contents.toString('utf8');
            }
        }))
        .pipe(rename({
            extname: '.html'
        }))
        .pipe(gulpIf(!isDevelopment, htmlmin({ collapseWhitespace: true })))
        .pipe(gulp.dest('public'))
})

gulp.task('build', gulp.series(gulp.parallel('styles', 'js', 'prep_html_opts'), 'link'));

gulp.task('watch', function () {
    gulp.watch('src/**/*.*', gulp.series(gulp.parallel('styles', 'js', 'prep_html_opts'), 'link'));
})

gulp.task('serve', function () {
    browserSync.init({
        server: "public"
    })

    browserSync.watch('public/**/*.*').on('change', browserSync.reload)
})

gulp.task('dev', gulp.series('build', gulp.parallel('watch', 'serve')));